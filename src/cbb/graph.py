import dgl
import numpy as np
import torch


STATS = [
    'Score', 'FGM', 'FGA', 'FGM3', 'FGA3', 'FTM',
    'FTA', 'OR', 'DR', 'Ast', 'TO', 'Stl', 'Blk', 'PF'
]
WINNER_STATS = [f"W{STAT}" for STAT in STATS]
LOSER_STATS = [f"L{STAT}" for STAT in STATS]


def map_loc(df):
    w_h_loc = df['WLoc'].map({'H': 1, 'A': 0, 'N': 0}).values.reshape(-1, 1)
    l_h_loc = df['WLoc'].map({'H': 0, 'A': 1, 'N': 0}).values.reshape(-1, 1)
    n_loc = df['WLoc'].map({'H': 0, 'A': 0, 'N': 1}).values.reshape(-1, 1)
    h_loc = np.vstack([w_h_loc, l_h_loc])
    n_loc = np.vstack([n_loc, n_loc])
    return np.hstack([h_loc, n_loc])


def df2graph(df, season, day, for_pred=False):
    max_id = df[['WTeamID', 'LTeamID']].values.max() + 1
    if for_pred:
        df = df[(df['Season'] == season) & (df['DayNum'] == day)]
    else:
        df = df[(df['Season'] == season) & (df['DayNum'] < day)]
    edges = df[['LTeamID', 'WTeamID']].values.tolist()
    rev_edges = df[['WTeamID', 'LTeamID']].values.tolist()
    stats = torch.from_numpy(df[WINNER_STATS].values.astype(np.float32))
    stats_opp = torch.from_numpy(df[LOSER_STATS].values.astype(np.float32))
    graph = dgl.graph(edges+rev_edges, num_nodes=max_id)
    graph.edata['stats'] = torch.cat([stats, stats_opp], dim=0)
    graph.edata['stats_opp'] = torch.cat([stats_opp, stats], dim=0)
    graph.edata['pts_diff'] = graph.edata['stats'][:, :1] - \
        graph.edata['stats_opp'][:, :1]
    graph.edata['day'] = torch.cat([torch.from_numpy(
        df[['DayNum']].values), torch.from_numpy(df[['DayNum']].values)])
    graph.edata['loc'] = torch.from_numpy(map_loc(df))
    return graph


class CBBGraphGenerator(object):
    def __init__(self):
        pass

    def __call__(self, df, season, day, for_pred=False):
        return df2graph(df, season, day, for_pred=for_pred)
