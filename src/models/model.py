import dgl
import torch


class GCNModel(torch.nn.Module):
    def __init__(self):
        super().__init__()

    def forward(self, graph, efeat):
        with graph.local_scope():
            graph.edata['h']